//
//  AddTagViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 19/05/22.
//

import UIKit
import CoreData

var tags = [Tags]()

let id : Int? = 0
let name : [String] = ["Work","Study","Reading","Break","Walk","Testing","Meeting","Shopping"]
let tagColors : [UIColor] = [UIColor(red: 1.00, green: 0.95, blue: 0.74, alpha: 1.00),
                          UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00),
                          UIColor(red: 0.75, green: 0.85, blue: 0.86, alpha: 1.00),
                          UIColor(red: 0.76, green: 0.88, blue: 0.77, alpha: 1.00),
                          UIColor(red: 0.83, green: 0.77, blue: 0.98, alpha: 1.00),
                          UIColor(red: 0.75, green: 0.83, blue: 0.95, alpha: 1.00),
                          UIColor(red: 0.98, green: 0.82, blue: 0.76, alpha: 1.00),
                          UIColor(red: 0.77, green: 0.87, blue: 0.96, alpha: 1.00)
                          
                          ]



class AddTagViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITextFieldDelegate {

    
    
    let label1: UILabel = {
            let l = UILabel(frame:CGRect.zero)
        l.text = "TAG NAME"
        
            l.font = UIFont.preferredFont(forTextStyle: .headline)
            l.translatesAutoresizingMaskIntoConstraints = false //enable autolayout
            return l
        }()
    let label2: UILabel = {
            let l = UILabel(frame:CGRect.zero)
        l.text = "CHOOSE TAG COLOR"
            l.font = UIFont.preferredFont(forTextStyle: .headline)
            l.translatesAutoresizingMaskIntoConstraints = false //enable autolayout
            return l
        }()


    let titleTextField:UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Enter tag name"
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.font = UIFont.systemFont(ofSize: 18)
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.layer.borderWidth = 0.1
        textField.layer.cornerRadius = 10
        textField.clearButtonMode = UITextField.ViewMode.whileEditing;
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.backgroundColor = .tertiarySystemBackground
        return textField
    }()
    
 
    @objc func addTag(){
        let name = titleTextField.text
        let id = Int64(tags.count + 1)
        createItem(id: id,name : name! ,color :color! )
        self.dismiss(animated: true,completion: nil)

    }
    

  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }

    var color : UIColor?

    let cellId = "CellId"; //Unique cell id

    let collectionView: UICollectionView = { // collection view to be added to view controller
//        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()); //zero size with flow layout
//
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset.right = 30
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.autoresizingMask = .flexibleWidth
        cv.translatesAutoresizingMaskIntoConstraints = false; //set it to false so that we can suppy constraints
        return cv;
    }();

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ADD TAG"
        navigationController?.navigationBar.prefersLargeTitles = true

        view.backgroundColor = .secondarySystemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(addTag))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        view.addSubview(label1)

        view.addSubview(titleTextField)
        view.addSubview(label2)

        
        label1.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        label1.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 60).isActive = true

        titleTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        titleTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 60).isActive = true
        titleTextField.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor,constant: -100).isActive = true
        titleTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        label2.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 40).isActive = true
        label2.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 60).isActive = true
        setupViews()
//        view.addSubview(button)
//        button.translatesAutoresizingMaskIntoConstraints = false
//        button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -30).isActive = true
//        button.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 70).isActive = true
//        button.widthAnchor.constraint(equalTo: titleTextField.widthAnchor,constant: -20).isActive = true
         
        getAllTags()
        
    }
    
   
    func setupViews() {
        view.addSubview(collectionView);
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .secondarySystemBackground
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId) //register collection view cell class
        
        collectionView.topAnchor.constraint(equalTo: label2.bottomAnchor,constant: 50).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: label2.leadingAnchor,constant: 0).isActive = true; //set the location of collection view
        collectionView.trailingAnchor.constraint(equalTo:  view.trailingAnchor).isActive = true; // top anchor of collection view
        collectionView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true; // height
        collectionView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true; // width
        
  
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
       cell.layer.cornerRadius = 10

   
       if indexPath.row == 0 {
           if indexPath.section == 0{
               cell.backgroundColor = UIColor(red: 1.00, green: 0.95, blue: 0.74, alpha: 1.00)

               return cell;

           }else if indexPath.section == 1{
               cell.backgroundColor = UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00)

               return cell;

           }else if indexPath.section == 2{
               cell.backgroundColor = UIColor(red: 0.75, green: 0.85, blue: 0.86, alpha: 1.00)
               return cell;

           }else{
               cell.backgroundColor = UIColor(red: 0.76, green: 0.88, blue: 0.77, alpha: 1.00)
               return cell;

           }
           
       }else{
           
           if indexPath.section == 0{

           cell.backgroundColor = UIColor(red: 0.83, green: 0.77, blue: 0.98, alpha: 1.00)

           return cell;

       }else if indexPath.section == 1{
           cell.backgroundColor = UIColor(red: 0.75, green: 0.83, blue: 0.95, alpha: 1.00)


           return cell;

       }else if indexPath.section == 2{
           cell.backgroundColor = UIColor(red: 0.98, green: 0.82, blue: 0.76, alpha: 1.00)
           return cell;

       }else{
           cell.backgroundColor = UIColor(red: 0.77, green: 0.87, blue: 0.96, alpha: 1.00)


           return cell;

       }

       }
   }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if indexPath.section == 0{
                color = UIColor(red: 1.00, green: 0.95, blue: 0.74, alpha: 1.00)
                print("pink")

            }else if indexPath.section == 1{
                color = UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00)

            }else if indexPath.section == 2{
                color = UIColor(red: 0.75, green: 0.85, blue: 0.86, alpha: 1.00)

            }else{
                color = UIColor(red: 0.76, green: 0.88, blue: 0.77, alpha: 1.00)

            }
            
        }else{
            
            if indexPath.section == 0{

                color = UIColor(red: 0.83, green: 0.77, blue: 0.98, alpha: 1.00)

        }else if indexPath.section == 1{
            color = UIColor(red: 0.75, green: 0.83, blue: 0.95, alpha: 1.00)



        }else if indexPath.section == 2{
            color = UIColor(red: 0.98, green: 0.82, blue: 0.76, alpha: 1.00)

        }else{
            color = UIColor(red: 0.77, green: 0.87, blue: 0.96, alpha: 1.00)



        }

    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    
        dismiss(animated: true, completion: nil)
            }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/4, height: view.frame.height/12);
    }

}
    
 }
    




func createItem(id: Int64,name : String,color : UIColor){
    let newItem = Tags(context: SecondViewController.context)
    newItem.id = id
    newItem.name = name
    newItem.color = color


    do{
        try SecondViewController.context.save()
        print("Success")
        getAllTags()
    }catch{
        print("Error")
    }
}
func getAllTags(){
do{
    
    
    let request = Tags.fetchRequest() as NSFetchRequest<Tags>
      
    
    tags = try SecondViewController.context.fetch(request)
    
    try SecondViewController.context.save()
    print(tags.count)
    for i in 0..<tags.count{
        print(tags[i].id)
        print(tags[i].name)

        print(tags[i].color)

    }
 
}catch {
    
}
}
